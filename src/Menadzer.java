import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio on 08.03.2017.
 */
public class Menadzer {

    private  List<Zadanie> lista=new ArrayList<Zadanie>();
    public Menadzer(Zadanie... arg){

        for (Zadanie z: arg) {
            lista.add(z);
        }
    }
    public void dodaj(Zadanie z){
        lista.add(z);
    }
    public  void wykonaj(){
        for (Zadanie z: lista ) {
            z.wykonaj();
            lista.remove(z);
        }
    }
}
