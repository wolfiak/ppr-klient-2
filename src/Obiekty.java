/**
 * Created by pacio on 08.03.2017.
 */
public class Obiekty {
    private String klucz;
    private Object wartosc;
    private boolean async;
    private long id;
    public Obiekty(String klucz,Object wartosc){
        this.klucz=klucz;
        this.wartosc=wartosc;
        async=false;
    }
    public Obiekty(String klucz,long id,Object wartosc){
        this.klucz=klucz;
        this.id=id;
        this.wartosc=wartosc;
        async=true;
    }
    public String getKlucz(){
        return klucz;
    }
    public Object getWartosc(){
        return wartosc;
    }
    public long getId(){
        return id;
    }
    public boolean getAsync(){
        return async;
    }


}
