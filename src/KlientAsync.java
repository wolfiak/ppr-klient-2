import java.io.*;
import java.net.ConnectException;
import java.net.Socket;

/**
 * Created by pacio on 23.03.2017.
 */
public class KlientAsync {
    private MenadzerK m;
    boolean decyzja=true;
    public KlientAsync(MenadzerK m ){
        this.m=m;
    }
    public  boolean LACZ(){
        for (Sasiad sa: Sasiad.sa) {
            Runnable r=new Runnable() {
                @Override
                public void run() {
                    boolean decyzja=true;
                    while(decyzja) {
                        Socket s = null;
                        try {
                            System.out.println(">> Klient Probowalem laczyc");
                            s = new Socket(sa.getIp(), sa.getPort());
                            decyzja = false;

                           // Info.Info(sa.getIp(), sa.getPort());
                            System.out.println("KLIENT>> Klient polaczony z serwerem (" + sa.getIp() + ":" + sa.getPort() + ")");
                            InputStream in = s.getInputStream();
                            OutputStream out = s.getOutputStream();
                            //DataInputStream dis = new DataInputStream(in);
                           // DataOutputStream dos = new DataOutputStream(out);
                            System.out.println("PRZED objectinputstream");
                            BufferedReader br=new BufferedReader(new InputStreamReader(in));
                            ObjectOutputStream oos = new ObjectOutputStream(out);
                            System.out.println("Za objectinputstream");
                            Tools.dodajObiekt(new Obiekty("oos", Thread.currentThread().getId(), oos));
                            Tools.dodajObiekt(new Obiekty("br", Thread.currentThread().getId(), br));
                           // Tools.dodajObiekt(new Obiekty("dis", dis));

                            m.wykonaj();

                            br.close();
                            oos.close();
                           // dos.close();
                           // dis.close();
                            out.close();
                            in.close();
                            s.close();

                        } catch (ConnectException e) {
                            System.err.println("Nie ma sasiadow");
                            try {
                                Thread.sleep(10000);
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            };
            Thread t=new Thread(r);
            t.start();

        }

        if(decyzja){
            System.out.println("Return true");
            return true;
        }else{
            System.out.println("Return false");
            return false;
        }

    }
}
