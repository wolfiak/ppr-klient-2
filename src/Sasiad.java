import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio on 22.03.2017.
 */
public class Sasiad implements Serializable{
    public static List<Sasiad> sa=new ArrayList<Sasiad>();
    private String ip;
    private String nazwa;
    private boolean nadwaca;
    private int port;
    public Sasiad(String ip,int port, String nazwa) {
        this.ip = ip;
        this.nazwa = nazwa;
        this.port=port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public boolean isNadwaca() {
        return nadwaca;
    }

    public void setNadwaca(boolean nadwaca) {
        this.nadwaca = nadwaca;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
