import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio on 08.03.2017.
 */
public  class Tools {
    private static List<Obiekty> obiekty=new ArrayList<Obiekty>();
    private static Menadzer m=new Menadzer();
    private static MenadzerK mk=new MenadzerK();
    public static boolean decyzja=true;
    public static void dodajZadnanie(Zadanie z){
        m.dodaj(z);
    }
    public static void dodajZadanieK(Zadanie z){
        System.out.println("Zadanie wyslane");
        mk.dodaj(z);}
    public static void wykonajZadania(){
        m.wykonaj();
    }
    public static void wykonajZadaniaK(){

        System.out.println("Jestem przed wykonywaniem zadann");
        mk.wykonaj();
    }
    public static void wyslij(DataOutputStream dos, String napis){
        try {
            byte[] data=napis.getBytes("UTF-8");
            dos.writeInt(data.length);
            dos.write(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String czytaj(DataInputStream dis){
        try {
          //  String napis="";
            int dlugosc=dis.readInt();

           System.out.println("Dlugosc odczytana: "+dlugosc);
            byte[] data=new byte[dlugosc];
            dis.readFully(data);

            return new String(data,"UTF-8");


        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }
    public static String czytajAsync(BufferedReader br){
        try {

            //int dlugosc=dis.readInt();

            // System.out.println("Dlugosc odczytana: "+dlugosc);
            //byte[] data=new byte[dlugosc];
            // dis.readFully(data);
            System.out.println(">> Czekam na teks");
            String napis=br.readLine();
            // return new String(data,"UTF-8");
            return napis;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }
    public static void dodajObiekt(Obiekty o){
        obiekty.add(o);
    }
    public static Object znajdzObiekt(String klucz){
        for (Obiekty o: obiekty) {
            if(o.getKlucz() == klucz){
                System.out.println("current: "+Thread.currentThread().getId()+" obiekt: "+o.getId());
                if(o.getAsync()==true && (Thread.currentThread().getId()==o.getId())) {
                    return o.getWartosc();
                }else {

                }
            }
        }
        return "";
    }
    public static void wyslijObiekt(Brodcast b){
        ObjectOutputStream oos= (ObjectOutputStream) Tools.znajdzObiekt("oos");

        try {
            oos.writeObject(b);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static Brodcast czytajObiekt(){
        ObjectInputStream ois= (ObjectInputStream) Tools.znajdzObiekt("ois");

        try {
            Brodcast br= (Brodcast) ois.readObject();

            return br;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void czysc(){
        obiekty.clear();
    }
}
