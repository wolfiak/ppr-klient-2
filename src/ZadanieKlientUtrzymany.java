import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by pacio on 29.03.2017.
 */
public class ZadanieKlientUtrzymany implements Zadanie{
    @Override
    public void wykonaj() {
        System.out.println("Tu sie dzieje magia");
        BufferedReader br= (BufferedReader) Tools.znajdzObiekt("br");
        ObjectOutputStream oos= (ObjectOutputStream) Tools.znajdzObiekt("oos");
       boolean work=true;
       boolean poczatek=true;
       int odp;
       while(true){
           try {
             odp=br.read();
             if(odp==0){
                 System.out.println("odp 0");
                oos.writeObject(new Sasiad(Info.Ip,Info.Port,Info.Imie));
             }else if(odp==1){
                 Tools.dodajZadanieK(new ZadanieOdbieranieBrodcast());
                 System.out.println("odp 1");
                 Tools.wykonajZadaniaK();
                 System.out.println("Zadanie wykonane");
             }
           } catch (IOException e) {
               e.printStackTrace();
           }
           try {
               Thread.sleep(5000);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
    }
}
