import java.util.Scanner;

/**
 * Created by pacio on 29.03.2017.
 */
public class Konsola {
    public static void start(){
        Scanner skaner=new Scanner(System.in);
        boolean dzialnie=true;
        System.out.println("Konsola aktywna");

        Runnable r=new Runnable() {
            @Override
            public void run() {
                while(dzialnie){
                    String wiad=skaner.nextLine();
                    switch(wiad){
                        case "help":{
                            System.out.println("Konsola jest aktywna");
                            System.out.println("---------------------------------------------------------------");
                            System.out.println("Komendy:");
                            System.out.println("wiad - Utworz nowa wiadomosc do pozostalych wezlow");


                            System.out.println("---------------------------------------------------------------");
                            break;
                        }
                        case "wiad":{
                            Tools.dodajZadnanie(new ZadanieGenerowanieWiadomosci());
                            break;
                        }
                    }
                }
            }
        };

        Thread t=new Thread(r);
        t.start();
    }
}
