import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by pacio on 29.03.2017.
 */
public class ZadanieUtrzymywaniePoalczenia implements  Zadanie{

    @Override
    public void wykonaj() {
        BufferedReader br = (BufferedReader) Tools.znajdzObiekt("br");
        BufferedWriter bw= (BufferedWriter) Tools.znajdzObiekt("bw");
        ObjectInputStream ois= (ObjectInputStream) Tools.znajdzObiekt("ois");
        Menadzer m=new Menadzer();
        boolean work=true;
        boolean poczatek=true;
        while(work){
            if(poczatek){
                System.out.println("<< Wysylanie komunikatu 0 do klienta w celu wyciagniecia jego danych");
                try {
                    bw.write(0);
                    Sasiad s=(Sasiad) ois.readObject();
                    System.out.println("Imie: "+s.getNazwa());
                    System.out.println("Port: "+s.getPort());

                    Sasiad.sa.add(s);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                poczatek=false;
            }
            try {
                bw.write(1);
                Tools.wykonajZadania();

                Thread.sleep(1000);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
